<?php

/**
 * Implements hook_services_resources().
 */
function og_services_services_resources() {
  $resources['og'] = array();
  // [POST] {endpoint}/og/{group_type}
  $resources['og']['create'] = array(
    'help' => 'Creates a group',
    'callback' => 'og_services_resource_create',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('create'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => 'The type of group to create.',
        'source' => array('path' => 0),
        'optional' => FALSE,
        'default value' => 'node',
      ),
      array(
        'name' => 'data',
        'type' => 'array',
        'description' => 'The group data',
        'source' => 'data',
        'optional' => FALSE,
      ),
    ),
  );
  // [GET] {endpoint}/og/{group_type}/{gid}
  $resources['og']['retrieve'] = array(
    'help' => 'Retrieves group information',
    'callback' => 'og_services_resource_retrieve',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('view'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => 'The entity type of the group to retrieve.',
        'source' => array('path' => 0),
        'optional' => FALSE,
        'default value' => 'node',
      ),
      array(
        'name' => 'gid',
        'type' => 'int',
        'description' => 'The gid of the group to retrieve.',
        'source' => array('path' => 1),
        'optional' => FALSE,
      ),
    ),
  );
  // [PUT] {endpoint}/og/{group_type}/{gid}
  $resources['og']['update'] = array(
    'help' => 'Updates a group.',
    'callback' => 'og_services_resource_update',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('update'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => 'The type of group being updated.',
        'source' => array('path' => 0),
        'optional' => FALSE,
        'default value' => 'node',
      ),
      array(
        'name' => 'gid',
        'type' => 'int',
        'description' => 'Group identifier.',
        'source' => array('path' => 1),
        'optional' => FALSE,
      ),
      array(
        'name' => 'data',
        'type' => 'array',
        'description' => 'The group data with updated information',
        'source' => 'data',
        'optional' => FALSE,
      ),
    ),
  );
  // [DELETE] {endpoint}/og/{group_type}/{gid}
  $resources['og']['delete'] = array(
    'help' => 'Deletes group',
    'callback' => 'og_services_resource_delete',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('delete'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => 'The type of group to delete.',
        'source' => array('path' => 0),
        'optional' => FALSE,
        'default value' => 'node',
      ),
      array(
        'name' => 'gid',
        'type' => 'int',
        'description' => 'The GID of the group to delete.',
        'source' => array('path' => 1),
        'optional' => FALSE,
      ),
    ),
  );
  // [GET] {endpoint}/og/{group_type}
  $resources['og']['index'] = array(
    'help' => 'Lists groups',
    'callback' => 'og_services_resource_index',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('index'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => 'The entity type of groups to list.',
        'source' => array('path' => 0),
        'optional' => FALSE,
        'default value' => 'node',
      ),
      array(
        'name' => 'page',
        'optional' => TRUE,
        'type' => 'int',
        'description' => 'The zero-based index of the page to get, defaults to 0.',
        'default value' => 0,
        'source' => array('param' => 'page'),
      ),
      array(
        'name' => 'fields',
        'optional' => TRUE,
        'type' => 'string',
        'description' => 'The fields to get.',
        'default value' => '*',
        'source' => array('param' => 'fields'),
      ),
      array(
        'name' => 'parameters',
        'optional' => TRUE,
        'type' => 'array',
        'description' => 'Parameters',
        'default value' => array(),
        'source' => array('param' => 'parameters'),
      ),
      array(
        'name' => 'pagesize',
        'optional' => TRUE,
        'type' => 'int',
        'description' => 'Number of records to get per page.',
        'default value' => variable_get('services_user_index_page_size', 20),
        'source' => array('param' => 'pagesize'),
      ),
    ),
  );
  // [POST] {endpoint}/og/join/{gid}/{uid}
  // [POST] {endpoint}/og/join/{gid}/{uid}/{group_type}
  $resources['og']['actions']['join'] = array(
    'help' => 'Subscribe a user to a group',
    'callback' => 'og_services_resource_user_join',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('subscribe'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => 'The type of group to join.',
        'source' => array('path' => 3),
        'optional' => TRUE,
        'default value' => 'node',
      ),
      array(
        'name' => 'gid',
        'type' => 'int',
        'description' => 'A gid of a group on the system',
        'source' => array('path' => 1),
        'optional' => FALSE,
      ),
      array(
        'name' => 'uid',
        'type' => 'int',
        'description' => 'A uid of a user on the system',
        'source' => array('path' => 2),
        'optional' => FALSE,
      ),
    ),

  );
  // [POST] {endpoint}/og/leave/{gid}/{uid}
  // [POST] {endpoint}/og/leave/{gid}/{uid}/{group_type}
  $resources['og']['actions']['leave'] = array(
    'help' => 'Unsubscribe a user from a group',
    'callback' => 'og_services_resource_user_leave',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('unsubscribe'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => 'The type of group to leave.',
        'source' => array('path' => 3),
        'optional' => TRUE,
        'default value' => 'node',
      ),
      array(
        'name' => 'gid',
        'type' => 'int',
        'description' => 'A gid of a group on the system',
        'source' => array('path' => 1),
        'optional' => FALSE,
      ),
      array(
        'name' => 'uid',
        'type' => 'int',
        'description' => 'A uid of a user on the system',
        'source' => array('path' => 2),
        'optional' => FALSE,
      ),
    ),
  );
  // [GET] {endpoint}/og/{gid}/users
  // [GET] {endpoint}/og/{gid}/users/{group_type}
  $resources['og']['relationships']['users'] = array(
    'help' => 'Show all users in a group',
    'callback' => 'og_services_resource_users',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('manage members'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => "The group's entity type.",
        'source' => array('path' => 2),
        'optional' => TRUE,
        'default value' => 'node',
      ),
      array(
        'name' => 'gid',
        'description' => 'A gid of a group on the system',
        'optional' => FALSE,
        'type' => 'int',
        'source' => array('path' => 0),
      ),
      array(
        'name' => 'page',
        'optional' => TRUE,
        'type' => 'int',
        'description' => 'The zero-based index of the page to get, defaults to 0.',
        'default value' => 0,
        'source' => array('param' => 'page'),
      ),
      array(
        'name' => 'fields',
        'optional' => TRUE,
        'type' => 'string',
        'description' => 'The fields to get.',
        'default value' => '*',
        'source' => array('param' => 'fields'),
      ),
      array(
        'name' => 'parameters',
        'optional' => TRUE,
        'type' => 'array',
        'description' => 'Parameters',
        'default value' => array(),
        'source' => array('param' => 'parameters'),
      ),
      array(
        'name' => 'pagesize',
        'optional' => TRUE,
        'type' => 'int',
        'description' => 'Number of records to get per page.',
        'default value' => variable_get('services_user_index_page_size', 20),
        'source' => array('param' => 'pagesize'),
      ),
    ),
  );

  // [GET] {endpoint}/user/{uid}/groups
  // [GET] {endpoint}/user/{uid}/groups/{group_type}
  $resources['user']['relationships']['groups'] = array(
    'help' => 'Show groups user is a member of.',
    'callback' => 'og_services_resource_user_memberships',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.resource'),
    'access callback' => 'og_services_user_access',
    'access arguments' => array('relate'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'group_type',
        'type' => 'string',
        'description' => "Filter by group entity type.",
        'source' => array('path' => 2),
        'optional' => TRUE,
      ),
      array(
        'name' => 'uid',
        'optional' => FALSE,
        'type' => 'int',
        'source' => array('path' => 0),
      ),
      array(
        'name' => 'page',
        'optional' => TRUE,
        'type' => 'int',
        'description' => 'The zero-based index of the page to get, defaults to 0.',
        'default value' => 0,
        'source' => array('param' => 'page'),
      ),
      array(
        'name' => 'fields',
        'optional' => TRUE,
        'type' => 'string',
        'description' => 'The fields to get.',
        'default value' => '*',
        'source' => array('param' => 'fields'),
      ),
      array(
        'name' => 'parameters',
        'optional' => TRUE,
        'type' => 'array',
        'description' => 'Parameters',
        'default value' => array(),
        'source' => array('param' => 'parameters'),
      ),
      array(
        'name' => 'pagesize',
        'optional' => TRUE,
        'type' => 'int',
        'description' => 'Number of records to get per page.',
        'default value' => variable_get('services_user_index_page_size', 20),
        'source' => array('param' => 'pagesize'),
      ),
    ),
  );

  $resources['og_membership'] = array();
  $resources['og_membership']['create'] = array(
    'help' => 'Creates a group membership',
    'callback' => 'og_membership_services_resource_create',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.membership'),
    'access callback' => 'og_services_og_membership_user_access',
    'access arguments' => array('create'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'data',
        'type' => 'array',
        'description' => 'The group membership data',
        'source' => 'data',
        'optional' => FALSE,
      ),
    ),
  );
  $resources['og_membership']['retrieve'] = array(
    'help' => 'Retrieves group information',
    'callback' => 'og_membership_services_resource_retrieve',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.membership'),
    'access callback' => 'og_services_og_membership_user_access',
    'access arguments' => array('retrieve'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'id',
        'type' => 'int',
        'description' => 'The id of the membership to retrieve.',
        'source' => array('path' => 0),
        'optional' => FALSE,
      ),
    ),
  );
  $resources['og_membership']['update'] = array(
    'help' => 'Updates a group membership',
    'callback' => 'og_membership_services_resource_update',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.membership'),
    'access callback' => 'og_services_og_membership_user_access',
    'access arguments' => array('update'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'id',
        'type' => 'int',
        'description' => 'Membership ID',
        'source' => array('path' => 0),
        'optional' => FALSE,
      ),
      array(
        'name' => 'data',
        'type' => 'array',
        'description' => 'The membership data with updated information',
        'source' => 'data',
        'optional' => FALSE,
      ),
    ),
  );
  $resources['og_membership']['delete'] = array(
    'help' => 'Deletes a group membership',
    'callback' => 'og_membership_services_resource_delete',
    'file' => array('type' => 'inc', 'module' => 'og_services', 'name' => 'og_services.membership'),
    'access callback' => 'og_services_og_membership_user_access',
    'access arguments' => array('delete'),
    'access arguments append' => TRUE,
    'args' => array(
      array(
        'name' => 'id',
        'type' => 'int',
        'description' => 'The ID of the group membership to delete.',
        'source' => array('path' => 0),
        'optional' => FALSE,
      ),
    ),
  );

  return $resources;
}

/**
 * Creates a group.
 *
 * @param array $data
 *   An associative array of fields and values to use in the creation of a group
 *   entity.
 *
 * @return
 *   The saved group.
 */
function og_services_resource_create($data) {
  if (!$data['entity_type']) {
    return services_error(t('Missing required field "entity_type".'), 404);
  }
  if (!$data['etid']) {
    return services_error(t('Missing required field "etid".'), 404);
  }
  $group = og_create_group($data);
  if ($group) {
    $group->save();
  }
  else {
    return services_error(t('Error creating group.'), 404);
  }
  return $group;
}

/**
 * Retrieves a group.
 *
 * @param string $group_type
 *   The entity type of the group to retrieve.
 * @param integer $gid
 *   The group identifier.
 */
function og_services_resource_retrieve($group_type, $gid) {
  $group = entity_load_single($group_type, $gid);

  if ($group) {
    $uri = entity_uri($group_type, $group);
    if ($uri) {
      $group->path = url($uri['path'], array('absolute' => TRUE));
    }
  }
  else {
    return services_error(t('There is no group with gid @gid.', array('@gid' => $gid)), 404);
  }

  if ($group_type == 'user') {
    services_remove_user_data($group);
  }
  $group = services_field_permissions_clean('view', $group_type, $group);
  return $group;
}

/**
 * Updates a group.
 *
 * @param string $group_type
 *   The entity type of the group to update.
 * @param integer $gid
 *   The entity ID of the group to update.
 * @param array $data
 *   An associative array of fields and values to apply to the group.
 *
 * @return
 *   The updated group.
 */
function og_services_resource_update($group_type, $gid, $data) {
  $group = entity_load_single($group_type, $gid);
  if (!$group) {
    return services_error(t('There is no group with gid @gid.', array('@gid' => $gid)), 404);
  }
  foreach ($data as $key => $value) {
    $group->{$key} = $value;
  }
  entity_save($group_type, $group);
  return $group;
}

/**
 * Deletes a group.
 *
 * @param string $group_type
 *   The entity type of the group to delete.
 * @param integer $gid
 *   The entity ID of the group to delete.
 *
 * @return
 *   FALSE, if there was no information on how to delete the entity.
 */
function og_services_resource_delete($group_type, $gid) {
  $group = entity_load_single($group_type, $gid);
  if (!$group) {
    return services_error(t('There is no group with gid @gid.', array('@gid' => $gid)), 404);
  }
  return entity_delete($group_type, $gid);
}

/**
 * Returns a group listing.
 */
function og_services_resource_index($group_type, $page, $fields, $parameters, $page_size) {
  $info = entity_get_info($group_type);
  $group_bundle_exists = FALSE;
  foreach ($info['bundles'] as $bundle => $bundle_info) {
    if (og_is_group_type($group_type, $bundle)) {
      $group_bundle_exists = TRUE;
      break;
    }
  }
  if (!$group_bundle_exists) {
    return services_error(t('There are no group types with the entity type of @group_type.', array(
      '@group_type' => $group_type,
    )), 404);
  }
  $select = db_select($info['base table'], 't')
    ->addTag($group_type . '_access');
  $select->join('field_data_' . OG_GROUP_FIELD, 'g', 't.' . $info['entity keys']['id'] . ' = g.entity_id AND g.entity_type = :entity_type', array(
    ":entity_type" => $group_type,
  ));

  if ($group_type == 'node' && !user_access('administer nodes')) {
    $select->condition('status', NODE_PUBLISHED);
  }

  services_resource_build_index_query($select, $page, $fields, $parameters, $page_size, 'group');
  $results = services_resource_execute_index_query($select);
  return services_resource_build_index_list($results, $group_type, $info['entity keys']['id']);
}

/**
 * Adds a user as a member to a group.
 *
 * @param string $group_type
 *   The entity type of the group to join.
 * @param integer $gid
 *   The ID of the group to join.
 * @param integer $uid
 *   The ID of the user joining the group.
 *
 * @return boolean
 *   TRUE if the user joined successfully, FALSE otherwise.
 */
function og_services_resource_user_join($group_type, $gid, $uid) {
  if (!user_load($uid)) {
    return services_error(t('Could not find a user with uid @uid.', array(
      "@uid" => $uid,
    )), 404);
  }
  $values = array(
    'entity_type' => 'user',
    'entity' => $uid,
    'field_name' => FALSE,
    'state' => og_user_access($group_type, $gid, 'subscribe without approval') ? 1 : (og_user_access($group_type, $gid, 'subscribe') ? 2 : NULL),
  );
  if (!$values['state']) {
    return FALSE;
  }
  og_group($group_type, $gid, $values);
  return og_is_member($group_type, $gid, 'user', user_load($uid));
}

/**
 * Removes a user's membership from a group.
 *
 * @param string $group_type
 *   The entity type of the group to leave.
 * @param integer $gid
 *   The ID of the group to leave.
 * @param integer $uid
 *   The ID of the user leaving the group.
 *
 * @return boolean
 *   TRUE if the user left successfully, FALSE otherwise.
 */
function og_services_resource_user_leave($group_type, $gid, $uid) {
  og_ungroup($group_type, $gid, 'user', $uid);
  return !og_is_member($group_type, $gid, 'user', user_load($uid));
}

function og_services_resource_users($group_type, $gid, $page, $fields, $parameters, $page_size) {
  $sub_select = db_select('og_membership', 't')
    ->condition('entity_type', 'user', '=')
    ->condition('group_type', $group_type)
    ->condition('gid', $gid, '=')
    ->orderBy('etid', 'ASC')
    ->extend('PagerDefault')
    ->limit($page_size);

  services_resource_build_index_query($sub_select, $page, $fields, $parameters, $page_size, 'og_membership');
  $results = services_resource_execute_index_query($sub_select);
  return services_resource_build_index_list($results, 'og_membership', 'gid');
}

function og_services_resource_user_memberships($group_type, $uid, $page, $fields, $parameters, $page_size) {
  $sub_select = db_select('og_membership', 't')
    ->condition('entity_type', 'user', '=')
    ->condition('etid', $uid, '=');
  if ($group_type) {
    $sub_select->condition('group_type', $group_type);
  }
  $sub_select->orderBy('gid', 'ASC')
    ->extend('PagerDefault')
    ->limit($page_size);

  services_resource_build_index_query($sub_select, $page, $fields, $parameters, $page_size, 'og_membership');
  $results = services_resource_execute_index_query($sub_select);
  return services_resource_build_index_list($results, 'og_membership', 'gid');
}

/**
 * Legacy function for getting a user's memberships.
 *
 * Used by drupalgap_og_services_request_postprocess_alter() without the
 * $group_type argument.
 *
 * @deprecated
 *   Use og_services_resource_user_memberships() instead.
 *
 * @see og_services_resource_user_memberships()
 * @see drupalgap_og_services_request_postprocess_alter()
 */
function _og_services_resource_user_memberships($uid, $page, $fields, $parameters, $page_size) {
  return og_services_resource_user_memberships("node", $uid, $page, $fields, $parameters, $page_size);
}
