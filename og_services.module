<?php

/**
 * Implements hook_ctools_plugin_api().
 */
function og_services_ctools_plugin_api($owner, $api) {
  if ($owner == 'services' && $api == 'services') {
    return array(
      'version' => 3,
      'file' => 'og_services.resource.inc',
    );
  }
}

/**
 * Determines if the current user can access an og resource.
 *
 * @param string $op
 *   The operation to be performed.
 * @param array $args
 *   Resource arguments passed through from the original request.
 *
 * @return boolean|ServicesError
 *   TRUE if the operations is permitted, FALSE if not, and a ServicesError if
 *   the reason is known.
 */
function og_services_user_access($op, $args = array()) {
  global $user;
  // Make sure we have an object or this all fails, some servers can mess up the
  // types.
  if (is_array($args[1])) {
    $args[1] = (object) $args[1];
  }
  elseif (!is_array($args[1]) && !is_object($args[1])) {
    $args[1] = (object)array('gid' => $args[1]);
  }

  // Get the group type.
  $group_type = $args[0];
  if ($op != 'relate') {
    $entity_info = entity_get_info();
    if (!isset($entity_info[$group_type])) {
      return services_error(t("@group_type is not a valid group type.", array(
        "@group_type" => $group_type,
      )), 406);
    }
  }

  // Get the group ID.
  // If $op is 'relate' then gid doesn't apply, but that's okay.  The case for
  // 'relate' will understand.
  if (isset($args[1]->gid)) {
    $gid = $args[1]->gid;
  }
  elseif ($group_type == 'node' && isset($args[1]->nid)) {
    $gid = $args[1]->nid;
  }
  else {
    $gid = NULL;
  }

  try {
    if ($op != 'create' && $op != 'index' && $op != 'relate' && !og_is_group($group_type, $gid)) {
      return services_error(t("@group_type @gid is not a group.", array(
        "@group_type" => $group_type,
        "@gid" => $gid,
      )), 406);
    }

    switch ($op) {
      case 'view':
        // Administrators and members may view their groups.
        return og_user_access($group_type, $gid, 'administer group') || og_is_member($group_type, $gid);
      case 'update':
        return og_user_access($group_type, $gid, 'update group');
      case 'create':
        if ($group_type == 'node' && isset($args[1]->type)) {
          return og_is_group_type($group_type, $args[1]->type) && node_access($op, $args[1]);
        }
        return entity_access($op, $group_type);
      case 'delete':
        $group = entity_load_single($group_type, $gid);
        return entity_access($op, $group_type, $group);
      case 'index':
        return TRUE;
      case 'manage members':
        return og_user_access($group_type, $gid, $op);

      case 'subscribe':
        $action = 'join';
      case 'unsubscribe':
        if (!isset($action)) {
          $action = 'leave';
        }
        // Users can only join/leave groups for themselves, unless they are an
        // admin.
        $uid = $args[2];
        if (!$uid) {
          return services_error(t('Anonymous users have no memberships.'), 403);
        }
        if ($user->uid != $uid && !og_user_access($group_type, $gid, 'administer group')) {
          return services_error(t('User @uid is not allowed to @action group @gid.', array(
            '@uid' => $uid,
            '@action' => $action,
            '@gid' => $gid
          )), 403);
        }
        if ($op == 'subscribe') {
          return og_user_access($group_type, $gid, $op) || og_user_access($group_type, $gid, 'subscribe without approval');
        }
        else if ($op == 'unsubscribe') {
          return og_user_access($group_type, $gid, $op);
        }
        break;

      case 'relate':
        // A user can get only his own groups unless he is an admin.
        if (user_is_anonymous()) {
          return services_error(t('Anonymous users have no groups.'), 403);
        }
        // Most resources use a gid, but in this case what was assumed to be a
        // gid is actually a uid.
        $uid = $gid;
        if ($user->uid != $uid && !user_access('administer group')) {
          return services_error(t('Not allowed to load groups for user @uid.', array('@uid' => $uid)), 403);
        }
        return TRUE;
    }

    // If we made it this far, just fall back to the group admin permission.
    return user_access('administer group');
  }
  catch (EntityMalformedException $e) {
    return services_error(t("@group_type @gid not found.", array(
      "@group_type" => $group_type,
      "@gid" => $gid,
    )), 404);
  }
}

/**
 * Access callback for og_membership resources.
 *
 * @param string $op
 *   The operation to be performed.
 * @param array $args
 *   Resource arguments passed through from the original request.
 *
 * @return boolean|ServicesError
 *   TRUE if the operations is permitted, FALSE if not, and a ServicesError if
 *   the reason is known.
 */
function og_services_og_membership_user_access($op, $args = array()) {
  $id = NULL;
  switch ($op) {
    case 'create':
      if (og_services_membership_has_valid_params($args[0])) {
        if (og_user_access($args[0]['group_type'], $args[0]['gid'], 'manage members')) {
          return TRUE;
        }
      }
      break;
    case 'retrieve':
    case 'update':
    case 'delete':
      $id = $args[0];
      break;
  }
  if ($id) {
    $membership = og_membership_load($id);
    if (og_user_access($membership->group_type, $membership->gid, 'manage members')) {
      return TRUE;
    }
  }

  // If we made it this far, just fall back to the group admin permission.
  return user_access('administer group');
}

function og_services_membership_has_valid_params($data) {
  $requiredParams = array(
    'entity_type',
    'etid',
    'group_type',
    'gid',
  );
  foreach ($requiredParams as $param) {
    if (!isset($data[$param])) {
      return services_error(t('Missing required field: @param.', array('@param' => $param)), 406);
    }
  }
  if (isset($data['type']) && !is_numeric($data['type'])) {
    return services_error(t('The value for type must be an integer'), 406);
  }
  return TRUE;
}
